use crate::common::{Byte, PageID, RecordData, RecordID};
use crate::constants::{FILE_HEADER_SIZE, MAX_PAGE_SIZE, PAGE_HEADER_SIZE};
use crate::page::{Page, PageHeader};
use std::convert::TryInto;
use std::io::prelude::*;
use std::{fs, mem, path, usize};

const FILE_BASE_DIRECTORY: &str = "data";

fn make_filepath(filepath: &str) -> String {
  format!("{}/{}", FILE_BASE_DIRECTORY, &filepath)
}

fn make_file_meta_path(filename: &str) -> String {
  format!("{}/{}/{}.meta", FILE_BASE_DIRECTORY, &filename, &filename)
}

pub struct File {
  name: String,
  header: FileHeader,
}

pub struct FileHeader {
  num_pages: usize,
  first_page_id: PageID,
}

impl FileHeader {
  pub fn serialize(file_header: &FileHeader) -> Vec<Byte> {
    let first_page_id = file_header.first_page_id.to_be_bytes();
    let num_pages = file_header.num_pages.to_be_bytes();
    let mut output = Vec::with_capacity(first_page_id.len() + num_pages.len());
    output.extend(&first_page_id);
    output.extend(&num_pages);

    output
  }
  pub fn deserialize(bytes: &[Byte]) -> FileHeader {
    const SIZE: usize = mem::size_of::<usize>();
    let first_page_id = usize::from_be_bytes(
      bytes[..SIZE]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let num_pages = usize::from_be_bytes(
      bytes[SIZE..2 * SIZE]
        .try_into()
        .expect("slice with incorrect length"),
    );

    FileHeader {
      first_page_id,
      num_pages,
    }
  }
}

impl File {
  pub fn open_or_create(filename: &str) -> Self {
    if Self::file_already_exists(filename) {
      Self::open(filename)
    } else {
      Self::create(filename)
    }
  }
  pub fn remove(filename: &str) -> () {
    fs::remove_dir_all(&make_filepath(filename))
      .expect(&format!("unable to remove file directory {}", &filename));
  }
}

impl File {
  pub fn insert_record(&mut self, record: &RecordData) -> RecordID {
    let mut free_page = None;
    for page_id in 1..=self.header.num_pages {
      let page = self.read_page(&page_id);
      if page.has_space_for_record(record) {
        free_page = Some(page);
      }
    }
    if let None = free_page {
      free_page = Some(self.allocate());
    }
    let mut free_page = free_page.unwrap();
    let record_id = free_page.insert_record(record).unwrap();
    self.write_page(&free_page);
    self.flush_meta();
    record_id
  }
  pub fn allocate(&mut self) -> Page {
    self.header.num_pages += 1;
    Page::new(self.header.num_pages)
  }
  pub fn write_page(&self, page: &Page) -> () {
    let filename = self.get_page_filename(&page.page_id());
    let mut file =
      fs::File::create(&filename).expect(&format!("unable to create file {}", &filename));
    file
      .write_all(&Self::serialize_page(&page))
      .expect(&format!("unable to write page {}", &page.page_id()));
  }
  pub fn read_page(&self, page_id: &PageID) -> Page {
    let filename = self.get_page_filename(page_id);
    let mut file = fs::File::open(&filename).expect(&format!("unable to open file {}", &filename));
    let mut buffer = Vec::with_capacity(MAX_PAGE_SIZE);
    buffer.resize_with(MAX_PAGE_SIZE, std::default::Default::default);

    file
      .read(&mut buffer)
      .expect(&format!("unable to read file {}", &filename));
    Self::deserialize_page(&buffer)
  }
  pub fn insert_record_to_page(&mut self, record: RecordData, page: &mut Page) -> () {
    let page_id = page.page_id();
    page
      .insert_record(record)
      .expect(&format!("unable to insert record to page {}", page_id));
  }
  pub fn name(&self) -> &str {
    &self.name
  }
}

impl File {
  fn create(filename: &str) -> Self {
    Self::mkdir_if_necessary(filename);
    let file = File {
      name: String::from(filename),
      header: FileHeader {
        num_pages: 0,
        first_page_id: 1,
      },
    };
    file.write_meta(&file.header);
    file
  }
  fn open(filename: &str) -> Self {
    File {
      name: String::from(filename),
      header: Self::read_meta(filename),
    }
  }
  fn file_already_exists(filename: &str) -> bool {
    std::path::Path::new(&make_filepath(filename))
      .join(format!("{}.meta", filename))
      .exists()
  }
  fn mkdir_if_necessary(filename: &str) -> () {
    let path_name = make_filepath(filename);
    let dir_path = path::Path::new(&path_name);
    if dir_path.exists() && dir_path.is_dir() {
      return;
    }
    fs::create_dir(&path_name).expect(&format!("unable to create file directory {}", &filename));
  }
  fn serialize_page(page: &Page) -> Vec<Byte> {
    let header = page.header.serialize();
    let mut output = Vec::with_capacity(header.len() + page.data.len());
    output.extend(&header);
    output.extend(&page.data);

    output
  }
  fn deserialize_page(bytes: &[Byte]) -> Page {
    let header = &bytes[..PAGE_HEADER_SIZE];
    let header = PageHeader::deserialize(header);
    let data = bytes[PAGE_HEADER_SIZE..].to_vec();

    Page { header, data }
  }
}

impl File {
  fn get_page_filename(&self, page_id: &PageID) -> String {
    let page_path = format!("page-{}.db", page_id);
    format!("{}/{}", &make_filepath(&self.name), page_path)
  }
  fn flush_meta(&self) -> () {
    self.write_meta(&self.header);
  }
  fn write_meta(&self, meta: &FileHeader) -> () {
    let filepath = make_file_meta_path(&self.name);
    let mut file =
      fs::File::create(&filepath).expect(&format!("unable to create file {}", &filepath));
    file
      .write_all(&FileHeader::serialize(meta))
      .expect(&format!("unable to write file header {}", &filepath));
  }
  fn read_meta(filename: &str) -> FileHeader {
    let filepath = make_file_meta_path(&filename);
    let mut file = fs::File::open(&filepath).expect(&format!("unable to open file {}", &filepath));
    let mut buffer = Vec::with_capacity(FILE_HEADER_SIZE);
    buffer.resize_with(buffer.capacity(), std::default::Default::default);

    file
      .read(&mut buffer)
      .expect(&format!("unable to read file {}", &filepath));
    FileHeader::deserialize(&buffer)
  }
}
