use crate::common::{Byte, PageID, RecordData, RecordID, SlotID};
use crate::constants::{INVALID_NUMBER, MAX_PAGE_DATA_SIZE, PAGE_HEADER_SIZE, PAGE_SLOT_SIZE};
use std::convert::TryInto;
use std::mem;
use std::usize;

#[derive(Debug)]
pub struct Page {
  pub header: PageHeader,
  pub data: Vec<Byte>,
}

#[derive(Debug)]
pub struct PageSlot {
  used: bool,
  offset: usize,
  length: usize,
}

#[derive(Debug)]
pub struct PageHeader {
  free_space_begin: usize,
  free_space_end: usize,
  num_slots: usize,
  num_free_slots: usize,
  current_page_id: PageID,
  next_page_id: PageID,
  prev_page_id: PageID,
}

impl PageHeader {
  pub fn serialize(&self) -> Vec<Byte> {
    let mut buffer = Vec::with_capacity(PAGE_HEADER_SIZE);
    buffer.append(&mut self.free_space_begin.to_be_bytes().to_vec());
    buffer.append(&mut self.free_space_end.to_be_bytes().to_vec());
    buffer.append(&mut self.num_slots.to_be_bytes().to_vec());
    buffer.append(&mut self.num_free_slots.to_be_bytes().to_vec());
    buffer.append(&mut self.current_page_id.to_be_bytes().to_vec());
    buffer.append(&mut self.next_page_id.to_be_bytes().to_vec());
    buffer.append(&mut self.prev_page_id.to_be_bytes().to_vec());

    buffer
  }
  pub fn deserialize(bytes: &[Byte]) -> Self {
    const SIZE: usize = mem::size_of::<usize>();
    let free_space_begin = usize::from_be_bytes(
      bytes[..SIZE]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let free_space_end = usize::from_be_bytes(
      bytes[SIZE..SIZE * 2]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let num_slots = usize::from_be_bytes(
      bytes[SIZE * 2..SIZE * 3]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let num_free_slots = usize::from_be_bytes(
      bytes[SIZE * 3..SIZE * 4]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let current_page_id = usize::from_be_bytes(
      bytes[SIZE * 4..SIZE * 5]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let next_page_id = usize::from_be_bytes(
      bytes[SIZE * 5..SIZE * 6]
        .try_into()
        .expect("slice with incorrect length"),
    );
    let prev_page_id = usize::from_be_bytes(
      bytes[SIZE * 6..SIZE * 7]
        .try_into()
        .expect("slice with incorrect length"),
    );

    Self {
      free_space_begin,
      free_space_end,
      num_slots,
      num_free_slots,
      current_page_id,
      next_page_id,
      prev_page_id,
    }
  }
}

impl PageSlot {
  pub fn new() -> Self {
    PageSlot {
      used: false,
      offset: 0,
      length: 0,
    }
  }
}

impl Page {
  ///
  /// Instantiates a new Page instance with reasonable default values.
  ///
  pub fn new(page_id: PageID) -> Self {
    Page {
      header: PageHeader {
        free_space_begin: 0,
        free_space_end: MAX_PAGE_DATA_SIZE,
        num_slots: 0,
        num_free_slots: 0,
        current_page_id: page_id,
        next_page_id: INVALID_NUMBER,
        prev_page_id: INVALID_NUMBER,
      },
      data: Self::initialize_empty_data_section(),
    }
  }
}

impl Page {
  /// Inserts a record into page.
  /// Returns inserted `record_id` if necessary free space is available,
  /// the error message otherwise.
  pub fn insert_record(&mut self, record_data: RecordData) -> Result<RecordID, &str> {
    if self.has_space_for_record(record_data) {
      let available_slot_id = self.get_available_slot_id();
      self.insert_record_of_slot(available_slot_id, record_data);
      Result::Ok(RecordID {
        page_id: self.header.current_page_id,
        slot_id: available_slot_id,
      })
    } else {
      Result::Err("page does not have necessary space")
    }
  }
  /// Checks whether there's space available for record to be inserted.
  ///
  /// ### Panics
  /// If record has no length.
  pub fn has_space_for_record(&self, record_data: RecordData) -> bool {
    let record_len = record_data.len();
    assert_ne!(
      record_len, 0,
      "why would you want to insert an empty record?"
    );

    let available_free_space = self.get_available_free_space();
    if self.header.num_free_slots > 0 {
      available_free_space >= record_data.len()
    } else {
      available_free_space >= record_data.len() + PAGE_SLOT_SIZE
    }
  }
  pub fn get_available_free_space(&self) -> usize {
    self.header.free_space_end - self.header.free_space_begin
  }
  pub fn page_id(&self) -> PageID {
    self.header.current_page_id
  }
  pub fn next_page_id(&self) -> PageID {
    self.header.next_page_id
  }
  pub fn print(&self) -> () {}
}

impl Page {
  /// Initialize a new **zero-filled** data vector with **maximum** available size.
  fn initialize_empty_data_section() -> Vec<Byte> {
    let mut empty_data = Vec::with_capacity(MAX_PAGE_DATA_SIZE);
    empty_data.resize(MAX_PAGE_DATA_SIZE, 0);
    empty_data
  }
  fn get_available_slot_id(&mut self) -> SlotID {
    let free_slot_id = if let Some(free_slot_id) = self.get_any_free_slot_id() {
      free_slot_id
    } else {
      self.grow_slot_directory();
      self.header.num_slots
    };
    free_slot_id
  }
  /// Inserts record to `data` and updates its respective `slot` entry.
  ///
  /// ### Panics
  /// - If passed `slot_id` is not in range `[1, page_num_slots]`.
  /// - If slot is already in use.
  fn insert_record_of_slot(&mut self, slot_id: SlotID, record_data: RecordData) -> () {
    assert!(self.validate_slot_id(slot_id), "not in range `slot_id`");
    assert!(!self.slot_is_in_use(slot_id), "slot is already in use");

    let (offset, length) = self.append_record_to_data(record_data);
    self.update_slot(slot_id, offset, length);
    self.header.free_space_end = offset;
  }
  fn get_any_free_slot_id(&self) -> Option<SlotID> {
    for slot_id in 1..=self.header.num_slots {
      if !self.slot_is_in_use(slot_id) {
        return Some(slot_id);
      }
    }
    None
  }
  fn grow_slot_directory(&mut self) -> () {
    self.header.num_slots += 1;
    self.header.free_space_begin += PAGE_SLOT_SIZE;
  }
  fn validate_slot_id(&self, slot_id: SlotID) -> bool {
    slot_id >= 1 && slot_id <= self.header.num_slots
  }
  fn slot_is_in_use(&self, slot_id: SlotID) -> bool {
    self.data[(slot_id - 1) * PAGE_SLOT_SIZE] == 1
  }
  fn append_record_to_data(&mut self, record_data: RecordData) -> (usize, usize) {
    let length = record_data.len();
    let offset = self.header.free_space_end - length;
    for (i, &byte) in record_data.iter().enumerate() {
      self.data[offset + i] = byte
    }
    (offset, length)
  }
  fn update_slot(&mut self, slot_id: SlotID, offset: usize, length: usize) {
    //
    // slot_data_section: [_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,]
    //                     ↑ |--------offset-------| |---------length------|
    //                   used
    let slot_start_index = (slot_id - 1) * PAGE_SLOT_SIZE;
    // mark slot as used
    self.data[slot_start_index] = 1;
    let offset = offset.to_be_bytes();
    let length = length.to_be_bytes();
    for (index, &byte) in offset.iter().chain(&length).enumerate() {
      self.data[slot_start_index + index + 1] = byte;
    }
  }
}
