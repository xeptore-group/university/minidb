use crate::common::Byte;

pub trait Serializable {
  fn serialize(&self) -> Vec<Byte>;
  fn deserialize(bytes: &[Byte]) -> Self;
}
