use crate::common::{PageID, SlotID};
use crate::file::FileHeader;
use crate::page::PageHeader;
use std::mem;

pub const MAX_PAGE_SIZE: usize = 8192;
pub const PAGE_HEADER_SIZE: usize = mem::size_of::<PageHeader>();
pub const FILE_HEADER_SIZE: usize = mem::size_of::<FileHeader>();
pub const PAGE_SLOT_SIZE: usize = mem::size_of::<usize>() * 2 + mem::size_of::<bool>();
pub const MAX_PAGE_DATA_SIZE: usize = MAX_PAGE_SIZE - PAGE_HEADER_SIZE;
pub const INVALID_NUMBER: PageID = 0;
pub const INVALID_SLOT: SlotID = 0;
