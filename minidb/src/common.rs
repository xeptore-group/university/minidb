pub type PageID = usize;
pub type SlotID = usize;
pub type Byte = u8;
pub type RecordData<'a> = &'a [Byte];

#[derive(Debug)]
pub struct RecordID {
  pub page_id: PageID,
  pub slot_id: SlotID,
}
