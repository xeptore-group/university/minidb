use minidb::common::Byte;
use minidb::serialize::Serializable;
use std::mem;

const ACCOUNT_SIZE: usize = mem::size_of::<char>() * 10 + mem::size_of::<usize>();
struct Account {
  number: [char; 10],
  credit: usize,
}

impl Account {
  fn serialize_number(&self) -> Vec<Byte> {
    let mut output = Vec::with_capacity(10 * mem::size_of::<char>());
    for ch in &self.number {
      let mut buffer = Vec::with_capacity(4);
      ch.encode_utf8(&mut buffer);
      output.extend(&buffer);
    }
    output
  }
}

impl Serializable for Account {
  fn serialize(&self) -> Vec<Byte> {
    let mut output = Vec::with_capacity(ACCOUNT_SIZE);
    output.extend(&self.credit.to_be_bytes());
    output.extend(&self.serialize_number());

    output
  }

  fn deserialize(bytes: &[Byte]) -> Self {
    Account {
      number: ['x'; 10],
      credit: 2500,
    }
  }
}

struct Card {
  number: [u8; 16],
  account_id: [char; 10],
  expiration_date: [char; 8],
}

struct Transaction<'a> {
  customer_id: &'a str,
  amount: usize,
}

struct Customer<'a> {
  name: &'a str,
  national_id: [char; 10],
}
